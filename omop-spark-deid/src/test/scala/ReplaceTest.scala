/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.omop.deid

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.QueryTest

class ReplaceTest extends QueryTest with SparkSessionTestWrapper with DeidTables {

  test("test replace basic") {
    spark.conf.set("spark.sql.crossJoin.enabled", true)
    val noteDeidTmp = DFTool.applySchema(spark.sql(
      """
      select 
      1 note_id
      , 1 person_id
      , 6 offset_begin
      , 11 offset_end
      , 'bob' lexical_variant
      """), schemaNoteNlp)

    val note = spark.sql(
      """
        select
        1 note_id
        , 'hello world' note_text
        """)

    val dfGold = DFTool.applySchema(spark.sql(
      """
      select 
      1 note_id
      , 'hello bob' note_text
      """), schemaNote)

    val dfResult = Replace.replace(spark, noteDeidTmp, note)

    checkAnswer(dfGold, dfResult)
  }

  test("test replace overlap") {
    spark.conf.set("spark.sql.crossJoin.enabled", true)
    val noteDeidTmp = DFTool.applySchema(spark.sql(
      """
      select 
      1 note_id
      , 1 person_id
      , 6 offset_begin
      , 8 offset_end
      , 'bob' lexical_variant
      union all
      select 
      1 note_id
      , 1 person_id
      , 6 offset_begin
      , 11 offset_end
      , 'bob' lexical_variant
      """), schemaNoteNlp)

    val note = spark.sql(
      """
        select
        1 note_id
        , 'hello world' note_text
        """)

    val dfGold = DFTool.applySchema(spark.sql(
      """
      select 
      1 note_id
      , 'hello bob' note_text
      """), schemaNote)

    val dfResult = Replace.replace(spark, noteDeidTmp, note)

    checkAnswer(dfGold, dfResult)
  }

  test("test replace overlap after") {
    spark.conf.set("spark.sql.crossJoin.enabled", true)
    val noteDeidTmp = DFTool.applySchema(spark.sql(
      """
      select 
      1 note_id
      , 1 person_id
      , 10 offset_begin
      , 13 offset_end
      , 'jim' lexical_variant
      union all
      select 
      1 note_id
      , 1 person_id
      , 6 offset_begin
      , 11 offset_end
      , 'bob' lexical_variant
      """), schemaNoteNlp)

    val note = spark.sql(
      """
        select
        1 note_id
        , 'hello world man' note_text
        """)

    val dfGold = DFTool.applySchema(spark.sql(
      """
      select 
      1 note_id
      , 'hello boban' note_text
      """), schemaNote)

    val dfResult = Replace.replace(spark, noteDeidTmp, note)

    checkAnswer(dfGold, dfResult)
  }

  test("test split phi list") {
    spark.conf.set("spark.sql.crossJoin.enabled", true)

    assert(Replace.extractPhiList("bOB, jim") == "bob" :: "jim" :: Nil)

  }

  test("test replace pseudomode") {
    spark.conf.set("spark.sql.crossJoin.enabled", true)
    val noteDeidTmp = DFTool.applySchema(spark.sql(
      """
      select
      1 note_id
      , 1 person_id
      , 10 offset_begin
      , 13 offset_end
      , 'jim' lexical_variant
      , 'nom' note_nlp_source_value
      union all
      select
      1 note_id
      , 1 person_id
      , 6 offset_begin
      , 11 offset_end
      , 'bob' lexical_variant
      , 'prenom' note_nlp_source_value
      """), schemaNoteNlp)

    val note = spark.sql(
      """
        select
        1 note_id
        , 'hello world man' note_text
        """)

    val dfGold = DFTool.applySchema(spark.sql(
      """
      select
      1 note_id
      , 'hello [prenom]an' note_text
      """), schemaNote)

    val dfResult = Replace.replace(spark, noteDeidTmp, note, "pseudo")

    checkAnswer(dfGold, dfResult)
  }

}

