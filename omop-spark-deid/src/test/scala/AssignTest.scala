/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.omop.deid

import io.frama.parisni.spark.dataframe._
import org.apache.spark.sql.QueryTest

class AssignTestextends extends QueryTest with SparkSessionTestWrapper  {


  test("test assign header") {

    val noteNlpNew = spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'HEADER' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'bob' snippet
    	, null lexical_variant
      """)

    val goldDf = DFTool.applySchema(spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'HEADER' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'bob' snippet
    	, '' lexical_variant
      """), Assign.schemaNoteNlp)

    val noteNlpOld = DFTool.createEmptyDataFrame(spark, Assign.schemaNoteNlp)

    val noteNlp = Assign.unionPastToPresent(noteNlpNew, noteNlpOld)
    val dfResult = Assign.assignHeader(noteNlp)
    //    noteNlp.show
    //    dfResult.show
    //    goldDf.show
    checkAnswer(goldDf, dfResult)
  }

  test("test assign prenom") {

    val noteNlpNew = spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'PRENOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'bob' snippet
    	, null lexical_variant
      """)

    val goldDf = DFTool.applySchema(spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'PRENOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'bob' snippet
    	, 'Jim' lexical_variant
      """), Assign.schemaNoteNlp)

    val dictPrenom = spark.sql(
      """
    select
     'jim' snippet
    """)

    val noteNlpOld = DFTool.createEmptyDataFrame(spark, Assign.schemaNoteNlp)

    val noteNlp = Assign.unionPastToPresent(noteNlpNew, noteNlpOld)
    val dfResult = Assign.assignOther(noteNlp, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom)
    //       noteNlp.show
    //       dfResult.show
    //       goldDf.show
    checkAnswer(goldDf, dfResult)
  }

  test("test assign prenom with old ") {

    val noteNlpNew = spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'PRENOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'bob' snippet
    	, null lexical_variant
      """)

    val goldDf = DFTool.applySchema(spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'PRENOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'bob' snippet
    	, 'Olivier' lexical_variant
      """), Assign.schemaNoteNlp)

    val dictPrenom = spark.sql(
      """
    select
     'jim' snippet
    union
    select
     'jim' snippet
    union
    select
     'Olivier' snippet
    """)

    val noteNlpOld = spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'PRENOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, 3 ids_deid_dico
    	, null term_temporal
    	, 'b' snippet
    	, 'Olivier' lexical_variant
      """)

    val noteNlp = Assign.unionPastToPresent(noteNlpNew, noteNlpOld)
    val dfResult = Assign.assignOther(noteNlp, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom)
    // noteNlp.show
    dfResult.show
    goldDf.show
    checkAnswer(goldDf, dfResult)
  }

  test("test assign prenom initial") {

    val noteNlpNew = spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'PRENOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null term_temporal
    	, 'B.' snippet
    	, null lexical_variant
      """)

    val goldDf = DFTool.applySchema(spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'PRENOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null term_temporal
    	, 'B.' snippet
    	, 'O.' lexical_variant
      """), Assign.schemaNoteNlp)

    val dictPrenom = spark.sql(
      """
    select
     'jim' snippet
    """)

    val noteNlpOld = spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'PRENOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null term_temporal
    	, 'b' snippet
    	, 'Olivier' lexical_variant
      """)

    val noteNlp = Assign.unionPastToPresent(noteNlpNew, noteNlpOld)
    val dfResult = Assign.assignOther(noteNlp, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom)
    //       noteNlp.show
    dfResult.show
    goldDf.show
    checkAnswer(goldDf, dfResult)
  }

  test("test assign nom") {

    val noteNlpNew = spark.sql(
      """
       select 
        1 as note_id
    	,	1 as person_id
    	,	'NOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'PAUL' snippet
    	, null lexical_variant
    	union
    	 select 
        2 as note_id
    	,	1 as person_id
    	,	'NOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'PAUL' snippet
    	, null lexical_variant
    	union
      select 
        1 as note_id
    	,	1 as person_id
    	,	'NOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'SIMON' snippet
    	, null lexical_variant
    	union
    	 select 
        2 as note_id
    	,	1 as person_id
    	,	'NOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'SIMON' snippet
    	, null lexical_variant
    	union
    	 select 
        3 as note_id
    	,	1 as person_id
    	,	'NOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'nico' snippet
    	, null lexical_variant
      """)

    val goldDf = DFTool.applySchema(spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'NOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'bob' snippet
    	, 'Jim' lexical_variant
    	union
    	select 
        2 as note_id
    	,	1 as person_id
    	,	'NOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'bob' snippet
    	, 'Jim' lexical_variant
      """), Assign.schemaNoteNlp)

    val dictPrenom = spark.sql(
      """
    select
     'jim' snippet
     union
    select
     'john' snippet
         union
    select
     'albert' snippet
         union
    select
     'adolfo kaminsky' snippet
    """)

    val noteNlpOld = spark.sql(
      """
      select 
        1 as note_id
    	,	1 as person_id
    	,	'NOM' as note_nlp_source_value
    	, 5 offset_begin
    	, 10 offset_end
    	, null ids_deid_dico
    	, null term_temporal
    	, 'SIMON' snippet
    	, null lexical_variant""")

    val noteNlp = Assign.unionPastToPresent(noteNlpNew, noteNlpOld)
    val dfResult = Assign.assignOther(noteNlp, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom, dictPrenom)
    noteNlp.show
    dfResult.show
    goldDf.show
    assert(dfResult.count == 5)
  }
}

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark session")
      .config("spark.sql.shuffle.partitions", "1")
      .getOrCreate()
  }

}
