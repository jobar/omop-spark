/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.parisni.omop.deid

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, lit, trim}

/** Factory for [[io.frama.parisni.omop.deid.Extract]] instances. */
object Extract extends DeidTables with DeidIO with App {

  val mode = args(0)
  val database = args(1)
  val source_database = args(2)
  val path = args(3)
  val pattern = args(4)
  val inputNote = args(5)
  val inputObservation = args(6)
  val inputLocation = args(7)
  val inputLocationHistory = args(8)
  val folderNum = args(9).toInt

  val spark = SparkSession
    .builder()
    .appName("OMOP DEID Extract")
    .enableHiveSupport()
    .getOrCreate()


  process(spark,
    mode,
    database,
    source_database,
    path,
    pattern,
    inputNote,
    inputObservation,
    inputLocation,
    inputLocationHistory)

  /**
   * Extract both the notes and the PHI contained in the omop database.
   * It also sanitize the notes and the PHI.
   *
   * @param spark                String Name of the hive database
   * @param mode                 String csv, delta or hive
   * @param database             String Name of the hive database
   * @param path                 String The path name to store the note content
   * @param pattern              String The folder pattern under the path
   * @param inputNote            String The folder pattern under the path
   * @param inputObservation     String The folder pattern under the path
   * @param inputLocation        String The folder pattern under the path
   * @param inputLocationHistory String The folder pattern under the path
   * @note TABLE OUTPUT:  [[KNOWLEDGE_TABLE]]
   * @note MISC OUTPUT: local folder gpu with texts
   * @todo use the standard concepts and related instead
   *       of the local codes
   */
  def process(spark: SparkSession,
              mode: String,
              database: String,
              source_database: String,
              path: String,
              pattern: String,
              inputNote: String,
              inputObservation: String,
              inputLocation: String,
              inputLocationHistory: String
             ) = {

    val input_note = readDataframe(spark, mode, database, inputNote)
    val input_observation = readDataframe(spark, mode, source_database, inputObservation)
    val input_location = readDataframe(spark, mode, source_database, inputLocation)
    val input_location_history = readDataframe(spark, mode, source_database, inputLocationHistory)

    val observation = getObservation(spark, input_observation, input_note, input_location, input_location_history)
    val note = getNote(spark, input_note)

    //create the json temp table
    writeTempDataframe(toJson(observation), DEID_KNOWLEDGE_TABLE)

    // export texts as files for neural network
    //generateFolders(path, pattern, folderNum)
    toLocal(note, path + java.io.File.separator + pattern, folderNum)

  }

  def getNote(spark: SparkSession, input_note: DataFrame) = {

    input_note.createOrReplaceTempView("note")
    // sanitize the string (remove special characters for gpu; otherwize it will fail silently)
    DFTool.applySchemaSoft(spark.sql(
      f"""
      SELECT
        note_id
      , regexp_replace(note_text, '[^\\p{L}\\s\\d\\p{Punct}]', ' ') as note_text
      FROM note
      """), schemaNote)
  }

  def getObservation(spark: SparkSession
                     , input_observation: DataFrame
                     , input_note: DataFrame
                     , input_location: DataFrame
                     , input_location_history: DataFrame): DataFrame = {

    input_note.createOrReplaceTempView("note")
    input_observation.createOrReplaceTempView("observation")

    // TODO: USE the standard concept ID, instead of the local source value
    DFTool.applySchemaSoft(spark.sql(
      f"""
      SELECT
        o.person_id
      , o.observation_concept_id
      , o.value_as_string
      , o.observation_source_value
      , o.observation_type_concept_id
      FROM observation o
      JOIN note ON (o.person_id = note.person_id)
      WHERE TRUE
      AND o.observation_type_concept_id = 44810199
      AND o.observation_source_value
      IN ( 'NOM', 'PRENOM', 'MAIL', 'TEL', 'SECU', 'ADRESSE', 'VILLE', 'ZIP')
       """), schemaObservation)
      .union(locationToObservation(spark, input_location, input_location_history, input_note))
      .withColumn("value_as_string", trim(col("value_as_string")))
      .dropDuplicates("person_id", "observation_type_concept_id", "observation_concept_id", "value_as_string")

  }

  def locationToObservation(sparkSession: SparkSession, location: DataFrame, locationHistory: DataFrame, note: DataFrame) = {

    val personLocation = locationHistory
      .filter(col("domain_id") === lit("Person"))
      .withColumnRenamed("entity_id", "person_id")
      .as("h")
      .join(location.as("l"), col("h.location_id") === col("l.location_id"), "left")
      .join(note.as("n"), col("n.person_id") === col("h.person_id"), "inner")
      .withColumn("observation_type_concept_id", lit(44810199))
      .select("h.person_id", "observation_type_concept_id", "address", "address_1", "address_2", "city", "zip")

    DFTool.applySchemaSoft(
      personLocation
        .selectExpr("person_id",
          "observation_type_concept_id",
          "address as value_as_string",
          "4160017 as observation_concept_id",
          "'ADRESSE' as observation_source_value")
        .filter(col("address").isNotNull)
        .union(personLocation
          .selectExpr("person_id",
            "observation_type_concept_id",
            "address_1 as value_as_string",
            "4160017 as observation_concept_id",
            "'ADRESSE' as observation_source_value")
          .filter(col("address_1").isNotNull))
        .union(personLocation
          .selectExpr("person_id",
            "observation_type_concept_id",
            "address_2 as value_as_string",
            "4160017 as observation_concept_id",
            "'ADRESSE' as observation_source_value")
          .filter(col("address_2").isNotNull))
        .union(personLocation
          .selectExpr("person_id",
            "observation_type_concept_id",
            "city as value_as_string",
            "4335813 as observation_concept_id",
            "'VILLE' as observation_source_value")
          .filter(col("city").isNotNull))
        .union(personLocation
          .selectExpr("person_id",
            "observation_type_concept_id",
            "zip as value_as_string",
            "4083591 as observation_concept_id",
            "'ZIP' as observation_source_value")
          .filter(col("ZIP").isNotNull))
      , schemaObservation)
  }

  /**
   * Put the Personal Informations into a json representation
   *
   * @param df DataFrame
   * @return DataFrame
   *
   */
  def toJson(df: DataFrame): DataFrame = {
    df.createOrReplaceTempView("t")
    val uima = "fr.aphp.wind.uima.type"
    val query =
      f"""
      with  
      a as (
      select 
        person_id
        , collect_list(named_struct('value', t.value_as_string, 'dataType', 'Name')) as ns 
        , '$uima.PatientFirstName' as nb
      from t where observation_concept_id IN ( 4086933, 44791058) group by person_id )
      ,b as (
      select 
        person_id
        , collect_list(named_struct('value', t.value_as_string, 'dataType', 'Name')) as ns 
        , '$uima.PatientLastName' as nb
      from t where observation_concept_id IN ( 4086449, 4307270 ) group by person_id )  
      ,c as (
      select 
        person_id
        , collect_list(named_struct('value', t.value_as_string, 'dataType', 'Name')) as ns 
        , '$uima.PatientEMail' as nb
      from t where observation_concept_id IN ( 4314148 ) group by person_id )
      ,d as (
      select 
        person_id
        , collect_list(named_struct('value', t.value_as_string, 'dataType', 'String')) as ns 
        , '$uima.PatientCityLive' as nb
      from t where observation_concept_id IN ( 4335813 ) group by person_id )
      ,e as (
      select 
        person_id
        , collect_list(named_struct('value', t.value_as_string, 'dataType', 'String')) as ns 
        , '$uima.PatientPhoneNumber' as nb
      from t where observation_concept_id IN ( 4083592 ) group by person_id ) 
      ,f as (
      select 
        person_id
        , collect_list(named_struct('value', t.value_as_string, 'dataType', 'String')) as ns 
        , '$uima.PatientZipCodeLive' as nb
      from t where observation_concept_id IN ( 4083591 ) group by person_id )
      ,g as (
      select 
        person_id
        , collect_list(named_struct('value', t.value_as_string, 'dataType', 'String')) as ns 
        , '$uima.PatientStreetNameLive' as nb
      from t where observation_concept_id IN ( 4160017 ) group by person_id )  
     ,i as (
      select 
        person_id
        , collect_list(named_struct('value', t.value_as_string, 'dataType', 'String')) as ns 
        , '$uima.PatientNbSecu' as nb
      from t where observation_concept_id IN ( 4245003 ) group by person_id )
      SELECT
          person_id
        , to_json( collect_list(
              named_struct( 'type', nb, 'values', ns )
            ) ) as json
      from (
      select * from a
      union all
      select * from b
      union all
      select * from c
      union all
      select * from d
      union all
      select * from e
      union all
      select * from f
      union all
      select * from g
      union all
      select * from i
      )
      group by person_id
      """

    df.sparkSession.sql(query)
  }

  /**
   * Produce a text file for every note
   * and dispatch it randomly in a folder.
   *
   * @param df     DataFrame The note dataframe
   * @param folder String The gpu folder with 12 slots
   * @param number Int Number of slots 12 slots
   * @return Unit
   *
   */
  def toLocal(df: DataFrame, folder: String, number: Int) = {

    df.collect().foreach(p => {
      val r = new java.util.Random();
      val random = r.nextInt(number);
      val fileName = folder + random + "/" + p.getLong(0).toString();
      val writer = new java.io.PrintWriter(fileName, "UTF-8");
      if (!p.isNullAt(1))
        writer.write(p.getString(1));
      writer.close();
    })
  }

}
