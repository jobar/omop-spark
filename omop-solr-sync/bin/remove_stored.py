from __future__ import print_function
import re
import sys


file = sys.argv[1]

f = open(file)
lines = f.readlines()
flag = False
for line in lines:
    if "<<BEGIN>>" in line:
        flag = True
    if "<<END>>" in line:
        flag = False
    if flag:
        print(re.sub(r"stored=\"true\"", "stored=\"false\"", line), end='')
    else:
        print(line, end='')

