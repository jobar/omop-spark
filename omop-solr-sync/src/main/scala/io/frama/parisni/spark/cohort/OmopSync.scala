/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.spark.cohort

import com.typesafe.scalalogging.LazyLogging
import io.delta.tables.DeltaTable
import io.frama.parisni.spark.postgres.PGTool
import io.frama.parisni.spark.solr._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.functions.{col, collect_list, max, broadcast, lit}
import org.apache.spark.sql.{DataFrame, SparkSession}

object OmopSync extends App with LazyLogging with OmopStructure {

  val ZK = args(0)
  val PG_COHORT = args(1)
  val DELTA_PATH = args(2)
  val TYPE = args(3)
  val LOG = args(4)
  val NOTE_CALC = args(5) // this contains note feature to be added
  val DELAY = if (args.size == 7) Some(args(6).toInt)
  else None

  logger.warn("DELAY = " + DELAY)
  val pgToDelta = TYPE.toLowerCase().contains("delta")
  val deltaToFhir = TYPE.toLowerCase().contains("fhir")
  val fhirToSolr = TYPE.toLowerCase().contains("solr")

  val options = Map("zkhost" -> f"$ZK", "commit_within" -> "10000", "batch_size" -> "5000", "timezone_id" -> "Europe/Paris")
  val urlCohort = f"${PG_COHORT}"
  val deltaPath = f"${DELTA_PATH}"

  logger.info("url zk: %s".format(options))
  logger.info("url cohort: %s".format(urlCohort))
  logger.info("url delta: %s".format(deltaPath))

  val spark = SparkSession.builder()
    .appName("Cohort 360 Sync")
    .getOrCreate()

  spark.sparkContext.setLogLevel(LOG)


  val pg = PGTool(spark, urlCohort, "spark-postgres-cohort")
  val noteCalc = spark.read.format("delta").load(NOTE_CALC)

  try {

    // TODO: optimize - merge only modified rows
    if (pgToDelta)
      updateDelta(pg, DELAY)

    // TODO: optimize - merge only modified rows
    if (deltaToFhir)
      updateFhirFromDelta(DELAY)

    // TODO: optimize - merge only modified rows
    if (fhirToSolr)
      updateSolrFromFhir(DELAY)

  } finally {
    pg.purgeTmp()
  }

  System.exit(0)

  /**
   *
   */
  def updateDelta(pg: PGTool, delay: Option[Int] = None) = {
    logger.warn("Syncing delta tables %s".format(if (delay.isDefined) {
      "with delay of " + delay.get + " minutes"
    } else {
      "from scratch"
    }))

    OMOP_TABLES.foreach(table => syncDelta(pg, deltaPath, table._1, table._2, delay))
  }

  def syncDelta(pg: PGTool, deltaPath: String, primaryKey: String, table: String, delay: Option[Int] = None) = {
    val delayFilter = if (delay.isDefined) "change_datetime >= now() - '%s minutes'::interval".format(delay.get)
    else "true"

    val candidate = pg.inputBulk("select * from %s where %s".format(table, delayFilter), Some(true), Some(8), Some(8), primaryKey)
    if (!candidate.isEmpty)
      scd1(candidate, table, primaryKey, deltaPath)
    else logger.warn("The candidate is empty")
  }

  def scd1(candidate: DataFrame, table: String, primaryKey: String, deltaPath: String) = {
    if (!tableExists(candidate.sparkSession, deltaPath, table)) {
      logger.warn("Table %s does not yet exists".format(deltaPath + table))
      candidate.write.mode(org.apache.spark.sql.SaveMode.Overwrite).format("delta").save(deltaPath + table)
    }
    else {
      logger.warn("Merging table %s with table of %d rows".format(deltaPath + table, candidate.count))
      DeltaTable.forPath(candidate.sparkSession, deltaPath + table)
        .as("t")
        .merge(
          candidate.as("s"),
          "s.%s = t.%s".format(primaryKey, primaryKey))
        .whenMatched("s.hash <> t.hash")
        .updateExpr(generateUpdate(candidate))
        .whenNotMatched()
        .insertExpr(generateUpdate(candidate))
        .execute()
    }
  }

  def generateUpdate(df: DataFrame) = {
    df.columns.map(c => {
      "`" + c + "`" -> ("s.`" + c + "`")
    }).toMap
  }

  def tableExists(spark: SparkSession, deltaPath: String, tablePath: String): Boolean = {
    val defaultFSConf = spark.sessionState.newHadoopConf().get("fs.defaultFS")
    val fsConf = if (deltaPath.startsWith("file:")) {
      "file:///"
    } else {
      defaultFSConf
    }
    val conf = new Configuration()
    conf.set("fs.defaultFS", fsConf)
    val fs = FileSystem.get(conf)

    fs.exists(new Path(deltaPath + tablePath))
  }

  def getCohort(): DataFrame = {

    val person = spark.read.format("delta").load(deltaPath + "person").select("person_id").alias("person")
    val cohort = spark.read.format("delta").load(deltaPath + "cohort").alias("cohort")
    val cohortList = person.join(cohort, col("person.person_id") === col("cohort.subject_id"), "left")
      .groupBy("person_id")
      .agg(collect_list("cohort_definition_id").alias("_list"), max(col("change_datetime")).as("change_datetime"))
      .select("person_id", "_list", "change_datetime")

    cohortList.cache

  }

  def getConceptFhir(): DataFrame = {

    spark.read.format("delta").load(deltaPath + "concept_fhir")
      .filter(col("concept_id").>=(lit(2000000000L)))
      .select("concept_id", "change_datetime", "concept_code", "concept_name"
        , "vocabulary_reference", "fhir_concept_code", "fhir_concept_name", "fhir_vocabulary_reference")
      .cache

  }

  /**
   *
   */
  def updateFhirFromDelta(delay: Option[Int] = None) = {

    logger.info("Updating Solr")

    val retConceptFhir = broadcast(getConceptFhir())

    // // patient related
    FHIRPatientSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIREncounterSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRClaimSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRConditionSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRProcedureSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRDocumentReferenceSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRCompositionSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir, noteCalc)

    // //  // not patient related
    FHIROrganizationSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRPractitionerSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRPractitionerRoleSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRGroupSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
    FHIRValueSetSolrSync(spark, options, urlCohort, deltaPath, delay).transformDelta(retConceptFhir)
  }

  def updateSolrFromFhir(delay: Option[Int] = None) = {

    logger.info("Updating Solr")

    val cohortDf = broadcast(getCohort())

    // // patient related
    FHIRPatientSolrSync(spark, options, urlCohort, deltaPath, delay).sync(cohortDf = Some(cohortDf))
    FHIRClaimSolrSync(spark, options, urlCohort, deltaPath, delay).sync(cohortDf = Some(cohortDf))
    FHIREncounterSolrSync(spark, options, urlCohort, deltaPath, delay).sync(cohortDf = Some(cohortDf))
    FHIRConditionSolrSync(spark, options, urlCohort, deltaPath, delay).sync(cohortDf = Some(cohortDf))
    FHIRProcedureSolrSync(spark, options, urlCohort, deltaPath, delay).sync(cohortDf = Some(cohortDf))
    FHIRDocumentReferenceSolrSync(spark, options, urlCohort, deltaPath, delay).sync(cohortDf = Some(cohortDf))
    FHIRCompositionSolrSync(spark, options, urlCohort, deltaPath, delay).sync(numPartitions = Some(1000), cohortDf = Some(cohortDf))

    // //  // not patient related
    FHIROrganizationSolrSync(spark, options, urlCohort, deltaPath, delay).sync()
    FHIRPractitionerSolrSync(spark, options, urlCohort, deltaPath, delay).sync()
    FHIRPractitionerRoleSolrSync(spark, options, urlCohort, deltaPath, delay).sync()
    FHIRGroupSolrSync(spark, options, urlCohort, deltaPath, delay).sync()
    //FHIRValueSetSolrSync(spark, options, urlCohort, deltaPath, delay).sync()

  }
}

