/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.spark.cohort

import io.frama.parisni.spark.postgres.PGTool
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import com.lucidworks.spark.util.SolrQuerySupport

/**
 * This validate that all of:
 * - postgres
 * - delta
 * - solr
 * are consistent.
 *
 * This includes:
 * - number of items are the same
 */
object OmopValidate extends App with OmopStructure {

  val deltaPath = args(0)
  val pgHost = args(1)
  val pgPort = args(2)
  val pgDb = args(3)
  val pgUser = args(4)
  val pgSchema = args(5)
  val zkHost = args(6)

  val spark = SparkSession.builder()
    .appName("Mapper loader")
    .master("yarn")
    .getOrCreate()

  val url = "jdbc:postgresql://%s:%s/%s?user=%s&currentSchema=%s"
    .format(pgHost, pgHost, pgDb, pgUser, pgSchema)
  val pg = PGTool.apply(spark, url, "spark-postgres-tmp")

  val pgCount = OMOP_TABLES.map {
    table => (table._2, getPostgresNumber(pg, table._2))
  }

  val deltaCount = OMOP_TABLES.map {
    table => (table._2, getDeltaNumber(spark, deltaPath, table._2))
  }

  val solrCount = OMOP_TABLES
    .filter(x => x._3 != None)
    .map {
      table => (table._2, getSolrNumber(spark, deltaPath, table._2))
    }

  printResult("delta", compareResult(pgCount, deltaCount))
  printResult("solr", compareResult(pgCount, solrCount))

  // report errors
  def printResult(source: String, result: List[(String, Boolean, Long, Long)]) = {
    result.foreach(
      x => println("[%s] Table <%s> is %sOK with %d items (over %d)"
        .format(source, x._1, if (x._2) ("NOT ") else (""), x._3, x._4))
    )
  }

  def compareResult(ref: List[Tuple2[String, Long]], result: List[Tuple2[String, Long]]) = {
    result.map {
      table =>
        (
          table._1,
          table._2 == ref.filter(x => x._1 == table._2).toList(0)._2,
          table._2,
          ref.filter(x => x._1 == table._2).toList(0)._2
        )
    }
  }

  def getPostgresNumber(pg: PGTool, tableName: String) = {
    pg.sqlExecWithResult("select count(1) as ct from (%s) as t".format(tableName)).select(col("ct"))
      .collect().map(x => x.getLong(0)).toList(0)
  }

  def getDeltaNumber(spark: SparkSession, path: String, tableName: String) = {
    spark.read.format("delta")
      .load(path + tableName).count
  }

  def getSolrNumber(spark: SparkSession, zkHost: String, collection: String) = {
    SolrQuerySupport.getNumDocsFromSolr(collection, zkHost, None)
  }

}
