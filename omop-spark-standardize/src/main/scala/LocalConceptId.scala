/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * This file is part of OMOP-SPARK.
 *
 * OMOP-SPARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OMOP-SPARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.interchu.omop

import com.typesafe.scalalogging.LazyLogging
import io.frama.interchu.omop.ConfigYaml._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

object LocalConceptId extends LazyLogging {

  /**
   * Make a lookup for each standard concept id
   * to the concept table
   *
   */

  def conceptIdsAll(dfMap: Map[String, DataFrame], conf: Config): Map[String, DataFrame] = {
    val result = collection.mutable.Map[String, DataFrame]()

    val cpt = dfMap.get("concept_pivot").get
    dfMap.filter(f => f._1 != "concept_pivot")
      .foreach(f => {
        result(f._1) = conceptIds(f._2, cpt, // now the list of fields of that table
          conf.resources.filter(d => d.name == f._1)(0).schema.fields)
      })

    dfMap ++ result.toMap
  }

  def conceptIds(df: DataFrame, cpt: DataFrame, fields: List[Fields]): DataFrame = {
    var tmp = df
    for (f <- fields) {
      if (f.name.matches(".*_source_value$")) {
        if (f.constraints.isDefined && f.constraints.get.localVocabularyId.isDefined) // if vocabulary_id is defined
          tmp = populateLocalConceptId(tmp, cpt, f.name, f.constraints.get.localVocabularyId.get)
      }
    }
    tmp
  }

  def populateLocalConceptId(df: DataFrame, cpt: DataFrame, columnLocal: String, vocabularyLocal: String): DataFrame = {
    val local_concept_id = getLocalConceptId(columnLocal)

    val cptVoc = cpt.where(lower(cpt("vocabulary_id")) === lower(lit(vocabularyLocal)))
      .select("concept_id", "concept_code")

    df.join(broadcast(cptVoc), lower(df(columnLocal)) === lower(cptVoc("concept_code")), "left")
      .withColumn(local_concept_id, coalesce(col("concept_id"), lit(0)))
      .drop("concept_id", "concept_code", "vocabulary_id")
  }

  def getLocalConceptId(c: String): String = {
    val pat = "^(.*?)_source_value$".r
    val pat(tmp) = c
    tmp + "_source_concept_id"
  }
}
