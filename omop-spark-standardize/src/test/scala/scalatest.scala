/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.frama.interchu.omop

import com.typesafe.scalalogging.LazyLogging
import io.frama.interchu.omop.ConfigYaml._
import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.QueryTest
import org.apache.spark.sql.types._

class AppTest extends QueryTest with SparkSessionTestWrapper with LazyLogging {


  test("test pipeline") {
    val filename = "test/config.yaml"
    val conf = ConfigYaml.getFromPath(filename)
    val structs = StructTypeGenerator.get(conf)

    val dfor = java.time.format.DateTimeFormatter.ofPattern("YYYYMMdd-HHmmss")
    val key = java.time.LocalDateTime.now().format(dfor)

    var dfMap = Initializer.init(spark, structs)

    Run.runPipeline(spark, dfMap, conf, structs)

  }

  test("test standardize standard concept id") {
    val df = spark.sql(
      """select
          1 person_id
        , 1 gender_source_concept_id
    		, 3 race_source_concept_id
    		union 
    		select 
    		    2 person_id
        , 1 gender_source_concept_id
    		, 3 race_source_concept_id
        """)

    val cpt = spark.sql(
      """select
          1 concept_id
          , 2 standard_concept_id
          , 'Person' standard_domain_id
          union all
          select
          3
          , 4 
          , 'Person' 
          """)

    val result = spark.sql(
      """select
            1 person_id
          , 1 gender_source_concept_id
          , 2 gender_concept_id
          , 3 race_source_concept_id
          , 4 race_concept_id
          
          union all
          
          select
            2 person_id
          , 1 gender_source_concept_id
          , 2 gender_concept_id
          , 3 race_source_concept_id
          , 4 race_concept_id
          """)

    val schema = StructType(StructField("person_id", LongType)
      :: StructField("gender_concept_id", LongType)
      :: StructField("gender_source_concept_id", LongType)
      :: StructField("race_concept_id", LongType)
      :: StructField("race_source_concept_id", LongType)
      :: Nil)

    val yamlTxt =
      """appName: OMOP standardization pipeline
inputFolder: test/input
outputFolder: test/output
tmpFolder: test/tmp
optionalSteps:
  qualityInput: true 
  qualityOutput: false 
  standardDispatch: true
  standardConcept: true 
  standardDate: true 
  localConcept: true 
  standardUnit: false 
resources:
- name: concept_pivot
  # csv | orc | parquet
  inputFormat: orc
  outputFormat: parquet
  active: true
  schema:
    fields:
    - name: concept_id
      type: integer
      constraints:
        required: true
        unique: true
    - name: standard_concept_id
      type: integer
    - name: standard_domain_id
      type: string
- name: person
  partition: 200 # optional
  # csv | orc | parquet
  inputFormat: orc
  outputFormat: orc
  active: true
  schema:
    fields:
    - name: person_id 
      type: bigint
      constraints:
        required: true
        unique: true
    - name: gender_concept_id
      type: integer
    - name: gender_source_value
      type: string
    - name: gender_source_concept_id  
      type: integer
      constraints:
        localVocabularyId: Gender
    - name: race_concept_id  
      type: integer
    - name: race_source_value
      type: string
    - name: race_source_concept_id  
      type: integer
        """
    val conf = ConfigYaml.getFromString(yamlTxt)
    val fields = conf.resources.filter(f => f.name == "person")(0).schema.fields
    val output = StandardConceptId.conceptIds(df, cpt, fields)

    checkAnswer(DFTool.applySchema(result, schema), DFTool.applySchema(output, schema))

  }

  test("test standardize local concept id") {
    //  spark.sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val df = spark.sql(
      """select
          1 person_id
    		, 'm' gender_source_value
        , 'm' race_source_value
        union all
         select    2 person_id
    		, 'm' gender_source_value
        , 'm' race_source_value
        """)

    val cpt = spark.sql(
      """select
            1 concept_id
          , 2 standard_concept_id
          , 'Person' standard_domain_id
    		  , 'Gender' vocabulary_id
    		  , 'm' concept_code 
    		  union all 
    		  select
    		   2 concept_id
          , 3 standard_concept_id
          , 'Person' standard_domain_id
    		  , 'Race' vocabulary_id
    		  , 'm' concept_code 
          """).coalesce(1)

    val result = spark.sql(
      """select
            1 person_id
          , 1 gender_source_concept_id
    		  , 'm' gender_source_value
    		  , 2 race_source_concept_id
    		  , 'm' race_source_value
    		  union all
    		  select
            2 person_id
          , 1 gender_source_concept_id
    		  , 'm' gender_source_value
    		  , 2 race_source_concept_id
    		  , 'm' race_source_value
          """)

    val schema = StructType(StructField("person_id", LongType)
      :: StructField("gender_source_concept_id", LongType)
      :: StructField("gender_source_value", StringType)
      :: StructField("race_source_concept_id", LongType)
      :: StructField("race_source_value", StringType)
      :: Nil)

    val yamlTxt =
      """appName: OMOP standardization pipeline
inputFolder: test/input
outputFolder: test/output
tmpFolder: test/tmp
optionalSteps:
  qualityInput: true 
  qualityOutput: false 
  standardDispatch: true
  standardConcept: true 
  standardDate: true 
  localConcept: true 
  standardUnit: false 
resources:
- name: concept_pivot
  # csv | orc | parquet
  inputFormat: orc
  outputFormat: parquet
  active: true
  schema:
    fields:
    - name: concept_id
      type: integer
      constraints:
        required: true
        unique: true
    - name: concept_code
      type: string
    - name: vocabulary_id
      type: string
    - name: domain_id
      type: string
    - name: standard_concept_id
      type: integer
- name: person
  partition: 200 # optional
  # csv | orc | parquet
  inputFormat: orc
  outputFormat: orc
  active: true
  schema:
    fields:
    - name: person_id 
      type: bigint
    - name: gender_concept_id
      type: integer
    - name: gender_source_value
      type: string
      constraints:
        localVocabularyId: Gender
    - name: gender_source_concept_id  
      type: integer
    - name: race_source_value
      type: string
      constraints:
        localVocabularyId: Race
    - name: race_source_concept_id  
      type: integer
        """

    val conf = ConfigYaml.getFromString(yamlTxt)
    val fields = conf.resources.filter(f => f.name == "person")(0).schema.fields
    val output = LocalConceptId.conceptIds(df, cpt, fields)
    val res = DFTool.applySchema(result, schema)
    val out = DFTool.applySchema(output, schema)
    checkAnswer(res, out)
  }

  test("test dispatcher") {

    val cpt = spark.sql(
      """select
          1 concept_id
          , 2 standard_concept_id
          , 'Procedure' standard_domain_id
          
          union all
          
          select
          3
          , 4 
          , 'Condition' 
          """)

    val procedure = spark.sql(
      """select
          1 procedure_occurrence_id
        , 1 procedure_source_concept_id

    		union 
    		
    		select 
    		  2 procedure_occurrence_id
        , 3 procedure_source_concept_id
        """)

    val condition = spark.sql(
      """
      select 
      2 as condition_occurrence_id
      , 3 as condition_source_concept_id
      
          """)
    val schema = StructType(StructField("condition_occurrence_id", LongType)
      :: StructField("condition_source_concept_id", LongType)
      :: Nil)

    val dfi = new DispatchFields(name = "condition_occurrence_id", from = Some("procedure_occurrence_id"), `type` = None)
    val dfii = new DispatchFields(name = "condition_source_concept_id", from = Some("procedure_source_concept_id"), `type` = None)
    val df = dfi :: dfii :: Nil
    val dt = new DispatchTo(name = "condition_occurrence", standardDomain = "condition", dispatchFields = df) :: Nil
    val d = new DispatchFrom(from = "procedure_occurrence", localDomain = "procedure", columnDispatch = "procedure_source_concept_id", to = dt)

    val procedureMaj = Dispatcher
      .addDomain(from = procedure, concept_pivot = cpt, dispatch = d, tempFolder = "test/tmp/")
    val conditionMaj = Dispatcher
      .transform(from = procedureMaj, dispatch = dt(0))

    checkAnswer(DFTool.applySchema(conditionMaj, schema), DFTool.applySchema(condition, schema))
  }

  test("test standardize date") {
    //  spark.sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val df = spark.sql(
      """select
          1                  person_id
    		, cast(null as date) birth_date
    		, CAST(UNIX_TIMESTAMP('08/26/2016 11:12:00', 'MM/dd/yyyy HH:mm:SS') AS TIMESTAMP)       birth_datetime
        """)

    val result = spark.sql(
      """select
            1                person_id
          , TO_DATE(CAST(UNIX_TIMESTAMP('08/26/2016', 'MM/dd/yyyy') AS TIMESTAMP))                birth_date
    		  , CAST(UNIX_TIMESTAMP('08/26/2016 11:12:00', 'MM/dd/yyyy HH:mm:SS') AS TIMESTAMP)       birth_datetime

          """)

    val schema = StructType(StructField("person_id", LongType)
      :: StructField("birth_date", DateType)
      :: StructField("birth_datetime", TimestampType)
      :: Nil)

    val yamlTxt =
      """appName: OMOP standardization pipeline
inputFolder: test/input
outputFolder: test/output
tmpFolder: test/tmp
optionalSteps:
  qualityInput: true 
  qualityOutput: false 
  standardDispatch: true
  standardConcept: true
  standardDate: true 
  localConcept: true 
  standardUnit: false 
resources:
- name: concept_pivot
  # csv | orc | parquet
  inputFormat: orc
  outputFormat: parquet
  active: true
  schema:
    fields:
    - name: concept_id
      type: integer
      constraints:
        required: true
        unique: true
    - name: concept_code
      type: string
    - name: vocabulary_id
      type: string
    - name: domain_id
      type: string
    - name: standard_concept_id
      type: integer
- name: person
  partition: 200 # optional
  # csv | orc | parquet
  inputFormat: orc
  outputFormat: orc
  active: true
  schema:
    fields:
    - name: person_id 
      type: bigint
    - name: birth_date
      type: date
    - name: birth_datetime
      type: timestamp

        """

    val conf = ConfigYaml.getFromString(yamlTxt)
    val fields = conf.resources.filter(f => f.name == "person")(0).schema.fields
    val output = StandardDate.date(df, fields)
    val res = DFTool.applySchema(result, schema)
    val out = DFTool.applySchema(output, schema)

    checkAnswer(res, out)
  }

  test("test quality output") {

    val df = spark.sql(
      """select
          1                  person_id
    		, cast(null as date) birth_date
    		, 'Jim_bob' name
    		, 'U' gender
    		, 124 age
        """)


    val yamlTxt =
      """appName: OMOP standardization pipeline
inputFolder: test/input
outputFolder: test/output
tmpFolder: test/tmp
optionalSteps:
  qualityInput: true 
  qualityOutput: true 
  standardDispatch: true
  standardConcept: true
  standardDate: true 
  localConcept: true 
  standardUnit: false 
resources:
- name: concept_pivot
  # csv | orc | parquet
  inputFormat: orc
  outputFormat: parquet
  active: false
  schema:
    fields:
    - name: concept_id
      type: integer
      constraints:
        required: true
        unique: true
    - name: concept_code
      type: string
    - name: vocabulary_id
      type: string
    - name: domain_id
      type: string
    - name: standard_concept_id
      type: integer
- name: person
  partition: 200 # optional
  # csv | orc | parquet
  inputFormat: orc
  outputFormat: orc
  active: false
  schema:
    fields:
    - name: person_id 
      type: bigint
      constraints:
        required: true
        unique: true
    - name: birth_date
      type: date
      constraints:
        required: true
        unique: true
    - name: name
      type: string
      constraints:
        pattern: jim.*
    - name: gender
      type: string
      constraints:
        enum:
        - M
          F
    - name: age
      type: bigint
      constraints:
        minimum: 0
        maximum: 120
  foreignKeys:
  - fields: [gender_concept_id]
    reference:
      fields: [concept_id]
      resource: concept_pivot
"""

    val conf = ConfigYaml.getFromString(yamlTxt)
    val fields = conf.resources.filter(f => f.name == "person")(0).schema.fields
    val checks = QualityOutput.generateConstraintsChecks(fields)
    logger.warn(checks.toString)
    val output = QualityOutput.validateChecks("person", df, checks)

  }

}

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark session")
      .config("spark.sql.shuffle.partitions", "1")
      .getOrCreate()
  }

}
