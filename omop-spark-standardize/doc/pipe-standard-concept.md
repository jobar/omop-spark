The `standard concept` pipe lookups for every fields ending with
`*_source_concept_id` and joins it against the **concept_pivot** table and gets
the standard `*_concept_id` if exists or `0`.
