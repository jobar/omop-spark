/**
 *     This file is part of SPARK-OMOP.
 *
 *     SPARK-OMOP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     SPARK-OMOP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
import com.typesafe.scalalogging._
import io.frama.parisni.spark.csv.CSVTool
import io.frama.parisni.spark.dataframe.DFTool
import io.frama.parisni.spark.postgres.PGTool
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

class DataFrameLoader(spark: SparkSession, pg: PGTool, conceptDf: Dataset[Row], relationshipDf: Dataset[Row], synonymDf: Dataset[Row], vocabularyDf: Dataset[Row], m_project_type_id: String, m_language_id: String, domain_id: String, vocabulary_id: String, perimeter: String) extends LazyLogging {

  def run() = {

    try {
      dropTmp()
      createTmp()
      loadTmp()
      purgeOmop()
      loadOmop()
    } finally {
      logger.warn("Removing tmp tables")
      dropTmp()
      pg.purgeTmp()
    }

  }

  def refresh() = {
    try {
      dropTmp()
      createTmp()
      loadTmp()
      if (perimeter == "concept")
        refreshOmop()
      if (perimeter == "relationship")
        refreshOmopRelationship()
    } finally {
      logger.warn("Removing tmp tables")
      dropTmp()
      pg.purgeTmp()
    }

  }

  def dropTmp() = {
    pg.tableDrop("concept_tmp")
    pg.tableDrop("concept_relationship_tmp")
    pg.tableDrop("concept_synonym_tmp")
    pg.tableDrop("vocabulary_tmp")
  }

  def purgeOmop() = {
    logger.warn("Purging previous data")
    val query = f"""
    WITH 
    del as (
      delete
      from concept 
      where lower(vocabulary_id ) in (select lower(vocabulary_id ) from concept_tmp)
      AND domain_id IN ( '$domain_id', 'Metadata' )
      AND m_project_id IN (select m_project_id from mapper_project where lower(m_project_type_id) = lower('$m_project_type_id'))
      returning concept_id
    ),
    del_statistic AS (
      delete from mapper_statistic where m_concept_id in (select concept_id from del)
    ),
    del_synonym AS (
      delete from concept_synonym where concept_id in (select concept_id from del)
    ),
    del_vocabulary AS (
      delete from vocabulary where lower(vocabulary_id ) in (select lower(vocabulary_id ) from concept_tmp)
    returning vocabulary_concept_id
    ),
    del_voc_concept AS (
    delete from concept where concept_id in (
    select vocabulary_concept_id from del_vocabulary)
    ),
    delete_rel1 as (
    DELETE
    FROM concept_relationship 
    WHERE concept_id_1 in (select concept_id from del)
    )
    DELETE
    FROM concept_relationship
    WHERE
    concept_id_2 in (select concept_id from del)
    """
    print(query)
    pg.sqlExec(query)

  }

  def createTmp() = {
    logger.warn("Creating temp tables")

    pg.sqlExec("""
    create unlogged table concept_tmp 
    (
        concept_name text
      , concept_code text not null
      , m_language_id text
      , m_project_type_id text
      , domain_id text
      , vocabulary_id text
      , m_statistic_type_id text
      , m_value_as_number double precision
      , concept_class_id text
    )
    """)

    pg.sqlExec("""
    create unlogged table concept_relationship_tmp 
    (
        concept_code text not null
      , vocabulary_id_target text not null
      , concept_code_target text not null
      , vocabulary_id text null
    	, relationship_id text null
    )
    """)

    pg.sqlExec("""
    create unlogged table vocabulary_tmp 
    (
        vocabulary_id text not null
      , vocabulary_name text not null
    	, vocabulary_reference text null
    	, vocabulary_version text null
    )
    """)

    pg.sqlExec("""
    create unlogged table concept_synonym_tmp 
    (
        concept_code text not null
      , concept_synonym_name text not null
    	, language_concept_id bigint not null
    )
    """)
  }
  def loadTmp() = {
    logger.warn("Loading tmp tables")

    pg.outputBulk("concept_tmp", conceptDf, 2)
    pg.outputBulk("concept_relationship_tmp", relationshipDf, 2)
    pg.outputBulk("concept_synonym_tmp", synonymDf, 2)
    pg.outputBulk("vocabulary_tmp", vocabularyDf, 2)
  }

  def refreshOmop() = {
    logger.warn("Refreshing concept tables")
    pg.sqlExec(f"""
    WITH 
    allc as (
      SELECT concept_name, coalesce(concept_code,'UNKNOWN') concept_code, m_language_id, coalesce(m_project_id, 1) as m_project_id, domain_id, vocabulary_id, now()::date valid_start_date, '20990101'::date valid_end_date, concept_tmp.concept_class_id
      FROM concept_tmp
      LEFT JOIN mapper_project ON lower(concept_tmp.m_project_type_id) = lower(mapper_project.m_project_type_id)
      ),
     ins as (
      INSERT INTO concept 
      (concept_name, concept_code, m_language_id, m_project_id, domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
      SELECT a.concept_name, a.concept_code, a.m_language_id, a.m_project_id, a.domain_id, a.vocabulary_id, a.valid_start_date, a.valid_end_date, a.concept_class_id
      FROM allc a
      LEFT JOIN concept on a.vocabulary_id = concept.vocabulary_id 
            AND a.concept_code = concept.concept_code
      WHERE concept.vocabulary_id IS NULL
      ),
      upt as (
      UPDATE concept 
      SET concept_name=a.concept_name, concept_code=a.concept_code, m_language_id=a.m_language_id, m_project_id=a.m_project_id, domain_id=a.domain_id, vocabulary_id=a.vocabulary_id, valid_start_date=a.valid_start_date, valid_end_date=a.valid_end_date, concept_class_id=a.concept_class_id
      FROM allc a
      WHERE concept.vocabulary_id = a.vocabulary_id 
      AND concept.concept_code = a.concept_code
      ),
      del as (
      UPDATE concept SET valid_end_date = now()
      WHERE 
      vocabulary_id IN (select vocabulary_id from concept_tmp)
      AND (vocabulary_id, concept_code) NOT IN (select vocabulary_id, concept_code from concept_tmp)
      )

      INSERT INTO mapper_job 
      (m_concept_id, m_job_type_id, m_status_id, m_user_id)
      SELECT concept.concept_id, 'MAPPING', 0 , 0 
      FROM concept_tmp
      JOIN concept on (concept.concept_code = concept_tmp.concept_code
      AND concept_tmp.vocabulary_id = concept.vocabulary_id)
      """)
  }
  
   def refreshOmopRelationship() = {
    logger.warn("Refreshing relationship tables")
    pg.sqlExec(f"""
    WITH 
       ins_relationship_to as (
      INSERT INTO concept_relationship 
      (concept_id_1, concept_id_2, relationship_id, valid_start_date, valid_end_date, m_user_id)
      SELECT c1.concept_id, c.concept_id, r.relationship_id, now(), '2099-01-01', 0
      FROM  concept_relationship_tmp r 
      JOIN concept c 
          ON (lower(c.concept_code) = lower(r.concept_code_target) 
              AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target)
              )
      JOIN concept c1 
          ON (lower(c1.concept_code) = lower(r.concept_code) 
              AND lower(c1.vocabulary_id) = lower(r.vocabulary_id)
              )
     LEFT JOIN concept_relationship c_r
          ON (lower(c_r.concept_id_1) = lower(c1.concept_id) 
              AND lower(c_r.concept_id_2) = lower(c.concept_id)
              AND lower(c_r.relationship_id) = lower(r.relationship_id)
              )
      WHERE c_r.concept_id_1 IS NULL
     returning concept_id_1
      )
   , ins_relationship_from as (
      INSERT INTO concept_relationship 
      (concept_id_1, concept_id_2, relationship_id, valid_start_date, valid_end_date, m_user_id)
      SELECT c.concept_id, c1.concept_id, rel.reverse_relationship_id, now(), '2099-01-01', 0
      FROM  concept_relationship_tmp r 
      JOIN relationship rel on (rel.relationship_id = r.relationship_id)
      JOIN concept c 
          ON (lower(c.concept_code) = lower(r.concept_code_target) 
              AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target)
              )
      JOIN concept c1 
          ON (lower(c1.concept_code) = lower(r.concept_code) 
              AND lower(c1.vocabulary_id) = lower(r.vocabulary_id)
              )
      LEFT JOIN concept_relationship c_r
          ON (lower(c_r.concept_id_1) = lower(c.concept_id) 
              AND lower(c_r.concept_id_2) = lower(c1.concept_id)
              AND lower(c_r.relationship_id) = lower(rel.relationship_id)
              )
      WHERE c_r.concept_id_1 IS NULL
      returning concept_id_1
      )
      
      INSERT INTO mapper_job 
      (m_concept_id, m_job_type_id, m_status_id, m_user_id)
      SELECT concept_id_1, 'MAPPING', 0 , 0 
      FROM ins_relationship_to
     
      UNION ALL
     
      SELECT concept_id_1, 'MAPPING', 0 , 0 
      FROM ins_relationship_from
      """)
  }

  def loadOmop() = {
    logger.warn("Loading tmp tables")

    logger.warn("Inserting into target omop tables")

    pg.sqlExec(f"""
    WITH 
     ins as (
      INSERT INTO concept 
      (concept_name, concept_code, m_language_id, m_project_id, domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
      SELECT concept_name, coalesce(concept_code,'UNKNOWN'), m_language_id, coalesce(m_project_id, 1) as m_project_id, domain_id, vocabulary_id, now()::date, '20990101', concept_tmp.concept_class_id
      FROM concept_tmp
      LEFT JOIN mapper_project ON lower(concept_tmp.m_project_type_id) = lower(mapper_project.m_project_type_id)
      RETURNING concept_id, concept_code
      )
   , voc_cpt as (
      INSERT INTO concept 
      (concept_name, concept_code, m_language_id, m_project_id, domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
      SELECT vocabulary_name, '$m_project_type_id' || ' ' || 'generated', '$m_language_id', coalesce(m_project_id, 0) as m_project_id, 'Metadata', '$m_project_type_id - Vocabulary', now()::date, '20990101', 'Vocabulary'
      FROM vocabulary_tmp
      LEFT JOIN mapper_project ON  lower('$m_project_type_id')= lower(mapper_project.m_project_type_id)
      WHERE lower(vocabulary_id  ) IN ( select lower(vocabulary_id ) from concept_tmp)
      RETURNING concept_id, concept_name, m_project_id
      )
  , ins_vocabulary as (
     INSERT INTO vocabulary
     (vocabulary_id, vocabulary_name, vocabulary_reference, vocabulary_version, vocabulary_concept_id, m_project_id)
     select vocabulary_id, vocabulary_name, vocabulary_reference, vocabulary_version, voc_cpt.concept_id, voc_cpt.m_project_id
     from vocabulary_tmp
     join voc_cpt on voc_cpt.concept_name = vocabulary_tmp.vocabulary_name
     WHERE lower(vocabulary_id  ) IN ( select lower(vocabulary_id ) from concept_tmp)
  )
  , ins_statistic as (
      INSERT INTO mapper_statistic 
      (m_concept_id, m_statistic_type_id, m_value_as_number, m_algo_id, m_user_id, m_valid_start_datetime)
      SELECT concept_id, m_statistic_type_id, m_value_as_number, 4, 10, now()
      FROM ins
      JOIN concept_tmp USING (concept_code)
      WHERE m_value_as_number is not null
      )
  , ins_relationship_to as (
      INSERT INTO concept_relationship 
      (concept_id_1, concept_id_2, relationship_id, valid_start_date, valid_end_date, m_user_id)
      SELECT ins.concept_id, c.concept_id, r.relationship_id, now(), '2099-01-01', 0
      FROM ins
      JOIN concept_relationship_tmp r ON (r.concept_code = ins.concept_code)
      JOIN concept c 
          ON (lower(c.concept_code) = lower(r.concept_code_target) 
              AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target)
              )
      )
   , ins_relationship_from as (
      INSERT INTO concept_relationship 
      (concept_id_1, concept_id_2, relationship_id, valid_start_date, valid_end_date, m_user_id)
      SELECT c.concept_id, ins.concept_id, rel.reverse_relationship_id, now(), '2099-01-01', 0
      FROM ins
      JOIN concept_relationship_tmp r ON (r.concept_code = ins.concept_code)
      JOIN relationship rel on (rel.relationship_id = r.relationship_id)
      JOIN concept c 
          ON (lower(c.concept_code) = lower(r.concept_code_target) 
              AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target)
              )
      )
    , ins_job as (
      INSERT INTO mapper_job 
      (m_concept_id, m_job_type_id, m_status_id, m_user_id)
      SELECT concept_id, 'MAPPING', 0 , 0 
     FROM ins)

      INSERT INTO concept_synonym
      (concept_id, concept_synonym_name, language_concept_id, m_user_id)
      SELECT ins.concept_id, concept_synonym_name, t.language_concept_id, 0
      FROM concept_synonym_tmp t
      JOIN ins ON (ins.concept_code = t.concept_code)

      
    """)

    val count = conceptDf.count
    logger.warn(f"Loaded: $count rows")
  }
}

object DataFrameLoader extends DataFrameToolkit with LazyLogging {

  def apply(spark: SparkSession, url: String, tmpFolder: String, csvPath: String, vocabularyDf: Dataset[Row], m_project_type_id: String, m_language_id: String, domain_id: String, vocabulary_id: String, delimiter: String, escape: String, mode: String, perimeter: String) = {
    val conceptDf = getConceptDf(spark, csvPath, delimiter, escape, m_project_type_id, m_language_id, domain_id, vocabulary_id)
    val synonymDf = getSynonymDf(spark, csvPath, delimiter, escape)
    val relationshipDf = getRelationshipDf(spark, csvPath, delimiter, escape, domain_id, vocabulary_id, m_project_type_id)
    val pg = PGTool(spark, url, tmpFolder)
    if (mode == "append")
      new DataFrameLoader(spark, pg, conceptDf, relationshipDf, synonymDf, vocabularyDf, m_project_type_id, m_language_id, domain_id, vocabulary_id, perimeter).refresh()
    else
      new DataFrameLoader(spark, pg, conceptDf, relationshipDf, synonymDf, vocabularyDf, m_project_type_id, m_language_id, domain_id, vocabulary_id, perimeter).run()
  }

  def getConceptDf(spark: SparkSession, csvPath: String, delimiter: String, escape: String, m_project_type_id: String, m_language_id: String, domain_id: String, vocabulary_id: String): Dataset[Row] = {
    val filePath = if (csvPath.matches(".*.csv$")) { csvPath } else { f"${csvPath}_concept.csv" }

    val defaultNull = new MetadataBuilder().putNull("default").build
    val default_m_language_id = new MetadataBuilder().putString("default", m_language_id).build
    val default_m_project_type_id = new MetadataBuilder().putString("default", m_project_type_id).build
    val default_domain_id = new MetadataBuilder().putString("default", domain_id).build
    val default_vocabulary_id = new MetadataBuilder().putString("default", vocabulary_id).build
    val default_m_statistic_type_id = new MetadataBuilder().putString("default", "FREQ").build
    val default_concept_class_id = new MetadataBuilder().putString("default", "EMPTY").build

    val schema = StructType(
      StructField("concept_name", StringType, false) ::
        StructField("concept_code", StringType, true, defaultNull) ::
        StructField("m_language_id", StringType, false, default_m_language_id) ::
        StructField("m_project_type_id", StringType, false, default_m_project_type_id) ::
        StructField("domain_id", StringType, false, default_domain_id) ::
        StructField("vocabulary_id", StringType, false, default_vocabulary_id) ::
        StructField("m_statistic_type_id", StringType, true, default_m_statistic_type_id) ::
        StructField("m_value_as_number", DoubleType, true, defaultNull) ::
        StructField("concept_class_id", StringType, false, default_concept_class_id) :: Nil)

    //var conceptTmp = DFTool.createEmptyDataFrame(spark, schema)
    var df = CSVTool(spark, path = filePath, delimiter = Some(delimiter), multiline = Some(true), escape = Some(escape), schema = schema, removeNullRows = Some("concept_name"))
    df = df
      .withColumn("concept_code", coalesce(col("concept_code"), col("concept_name")))
      .withColumn( // append PROJECT - toVocabulary
        "vocabulary_id",
        when(col("vocabulary_id").rlike("^" + m_project_type_id), col("vocabulary_id"))
          .otherwise(concat(lit(m_project_type_id + " - "), col("vocabulary_id"))))

    var conceptTmp = removeNullRows(df, "concept_name")
    conceptTmp = removeDuplicate(conceptTmp, "concept_code")
    logger.warn(f"Concept table is valid")
    conceptTmp
  }

  def getRelationshipDf(spark: SparkSession, csvPath: String, delimiter: String, escape: String, domain_id: String, vocabulary_id: String, m_project_type_id: String): Dataset[Row] = {
    val filePath = f"${csvPath}_relationship.csv"
    val default_relationship_id = new MetadataBuilder().putString("default", "Maps to").build
    val default_vocabulary_id = new MetadataBuilder().putString("default", vocabulary_id).build
    val schema = StructType(
      StructField("concept_code", StringType, true) ::
        StructField("vocabulary_id_target", StringType, false) ::
        StructField("concept_code_target", StringType, false) ::
        StructField("vocabulary_id", StringType, true, default_vocabulary_id) ::
        StructField("relationship_id", StringType, true, default_relationship_id)
        :: Nil)

    val a = new java.io.File(filePath);
    if (!a.exists()) {
      logger.warn(f"No relationship table")
      DFTool.createEmptyDataFrame(spark, schema)
    } else {
      var result = CSVTool(spark, path = filePath, delimiter = Some(delimiter), multiline = Some(true), escape = Some(escape), schema = schema, removeNullRows = Some("concept_code_target"))
      logger.warn(f"Relationship table is valid")
      result = removeNullRows(result, "concept_code")
      result = result.withColumn( // append PROJECT - toVocabulary
        "vocabulary_id",
        when(col("vocabulary_id").rlike("^" + m_project_type_id), col("vocabulary_id"))
          .otherwise(concat(lit(m_project_type_id + " - "), col("vocabulary_id"))))
      // remove duplicates relationships
      result = removeDuplicate(result, "concept_code", "vocabulary_id", "concept_code_target", "relationship_id", "vocabulary_id_target")
      result
    }
  }

  def getSynonymDf(spark: SparkSession, csvPath: String, delimiter: String, escape: String): Dataset[Row] = {
    val filePath = f"${csvPath}_synonym.csv"
    val schema = StructType(
      StructField("concept_code", StringType, false) ::
        StructField("concept_synonym_name", StringType, false) ::
        StructField("language_concept_id", LongType, false)
        :: Nil)

    val a = new java.io.File(filePath);
    if (!a.exists()) {
      logger.warn(f"No synonym table")
      DFTool.createEmptyDataFrame(spark, schema)
    } else {
      val result = CSVTool(spark, path = filePath, delimiter = Some(delimiter), multiline = Some(true), escape = Some(escape), schema = schema)
      logger.warn(f"Synonym table is valid")
      result
    }
  }
}

trait DataFrameToolkit {

  def removeNullRows(df: Dataset[Row], column: String): Dataset[Row] = {
    df.registerTempTable("nullTmp")
    val spark = df.sparkSession
    var nulltmp = spark.sql(f"select * from nullTmp where $column IS NULL and trim($column) != ''")
    println(nulltmp.count + " missing rows")
    spark.sql(f"select * from nullTmp where $column IS NOT NULL and trim($column) !=''")
  }

  def removeDuplicate(df: Dataset[Row], column: String*): Dataset[Row] = {
    val tmp = df.dropDuplicates(column)

    val diff = df.count - tmp.count
    if (diff > 0) {
      println(f"removed $diff rows")
      df.except(tmp).show
    }
    tmp
  }
}

object Run extends App with LazyLogging {
  val master = args(0)
  val tmpFolder = args(1)
  val TERM_PATH = args(2)

  val FILE_NAME = args(3)
  val VOC_FILE_NAME = args(4)
  val mode = args(5).toString()
  val perimeter = args(6).toString()

  val modeMode = ("overwrite" :: "append" :: Nil)
  val perimeterMode = ("concept" :: "relationship" :: "synonym" :: "full" :: Nil)
  require(modeMode.contains(mode), "Mode should be in %s".format(modeMode.mkString(",")))
  require(perimeterMode.contains(perimeter), "Perimeter should be in %s".format(perimeterMode.mkString(", ")))
  require(("append" :: Nil).contains(mode) || (("overwrite" :: Nil).contains(mode) && ("full" :: Nil).contains(perimeter)), "Overwrite is only on Full mode")
  require(("overwrite" :: Nil).contains(mode) || (("append" :: Nil).contains(mode) && ("concept" :: "relationship" :: Nil).contains(perimeter)), "Append mode is only with concept or relationship")

  val spark = SparkSession.builder()
    .appName("Mapper loader")
    .master(master)
    .getOrCreate()

  val url = "jdbc:postgresql://localhost:5432/mimic?user=mapper&currentSchema=map"
  val file = new java.io.File(f"${TERM_PATH}/${FILE_NAME}")

  var conf = spark.read.option("inferSchema", "true").option("header", "true").format("csv").load(file.getAbsolutePath)
  var it = conf.rdd.toLocalIterator
  while (it.hasNext) {
    var value = it.next
    println(value)
    var active = value.getAs("active").toString
    var file = value.getAs("file").toString
    var m_project_type_id = value.getAs("project").toString
    var domain_id = value.getAs("domain").toString
    var vocabulary_id = value.getAs("vocabulary").toString
    var m_language_id = value.getAs("language").toString
    // var mode = value.getAs("mode").toString
    var delimiter = value.getAs("sep").toString
    var escape = value.getAs("quote").toString
    // TODO: do not read the vocabulary every run
    var csvPath = TERM_PATH + m_project_type_id.toLowerCase + "/" + file
    var vocCsvPath = TERM_PATH + m_project_type_id.toLowerCase + "/" + VOC_FILE_NAME

    val vocabularyDf = getVocabularyDf(spark, vocCsvPath, m_project_type_id)

    if (active == "1")
      DataFrameLoader(spark, url, tmpFolder, csvPath, vocabularyDf, m_project_type_id, m_language_id, domain_id, vocabulary_id, delimiter, escape, mode, perimeter)
  }

  def getVocabularyDf(spark: SparkSession, csvPath: String, m_project_type_id:String): Dataset[Row] = {

    val schema = StructType(
      StructField("vocabulary_id", StringType, false) ::
        StructField("vocabulary_name", StringType, false) ::
        StructField("vocabulary_reference", StringType, true) ::
        StructField("vocabulary_version", StringType, true)
        :: Nil)

    val a = new java.io.File(csvPath);
    if (!a.exists()) {
      logger.error(f"No vocabulary table")
      DFTool.createEmptyDataFrame(spark, schema)
    } else {
      val result = CSVTool(spark, path = csvPath, schema = schema, multiline = Some(true))
      logger.warn(f"Vocabulary table is valid")
      result.withColumn( // append PROJECT - toVocabulary
        "vocabulary_id",
        when(col("vocabulary_id").rlike("^" + m_project_type_id), col("vocabulary_id"))
          .otherwise(concat(lit(m_project_type_id + " - "), col("vocabulary_id"))))
    }
  }
}

