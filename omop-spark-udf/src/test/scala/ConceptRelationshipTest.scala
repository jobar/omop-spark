/**
 * This file is part of SPARK-OMOP.
 *
 * SPARK-OMOP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SPARK-OMOP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.frama.parisni.spark.omop.graph

import org.apache.spark.sql.QueryTest

class ConceptRelationshipTest extends QueryTest with SparkSessionTestWrapper {


  test("test concept relationship join") {
    import spark.implicits._
    val data = (
      (1, 1)
        :: (1, 2)
        :: (1, 3)
        :: (1, 4)
        :: Nil)
      .toDF("concept_id_1", "concept_id_2")

    val res = ConceptRelationshipCalc.generateCandidate(data)

    res.show(1000, false)
    assert(res.count() === 12)
  }

  test("test remove exist") {
    import spark.implicits._

    val data = (
      (1, 1)
        :: (1, 2)
        :: (1, 3)
        :: (1, 4)
        :: Nil)
      .toDF("concept_id_1", "concept_id_2")

    val candidate = ConceptRelationshipCalc.generateCandidate(data)
    val res = ConceptRelationshipCalc.removeExistingCandidate(candidate, data)
    res.show
    assert(res.count() === 9)

  }

}

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark session")
      .config("spark.sql.shuffle.partitions", "1")
      .getOrCreate()
  }

}
